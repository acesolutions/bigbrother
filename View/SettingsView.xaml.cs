﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfUtils;

namespace OpenALPRDemo.View
{
	/// <summary>
	/// Interaction logic for SettingsView.xaml
	/// </summary>
	public partial class SettingsView : Window
	{
		public SettingsView()
		{
			InitializeComponent();
			this.DataContext = Properties.Settings.Default;
		}

		private void OkButton_Click(object sender, RoutedEventArgs e)
		{
			Properties.Settings.Default.Save();
			this.Close();
		}

		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		private void FFmpegBrowse_Click(object sender, RoutedEventArgs e)
		{
			string ffmpegPath = Dialogs.OpenFile("Executable (*.exe)|*.exe", "Select FFmpeg executable");
			if (ffmpegPath != null)
			{
				Properties.Settings.Default.FFmpegPath = ffmpegPath;
			}
		}

		private void ScreenshotPathBrowse_Click(object sender, RoutedEventArgs e)
		{
			string screenshotPath = Dialogs.SelectFolder("Select a folder to store the screenshots in.");
			if (screenshotPath != null)
			{
				Properties.Settings.Default.ScreenshotPath = screenshotPath;
			}
		}
	}
}
