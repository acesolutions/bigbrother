﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OpenALPRDemo
{
	public class FilenameGeneratedEventArgs : EventArgs
	{
		public string Filename { get; }

		public FilenameGeneratedEventArgs(DateTime theTime, string theExtension)
		{
			this.Filename = theTime.ToString("yyyyMMdd-HHmmss") + theExtension;
		}
	}

	public class TimerFilenameGenerator
	{
		private Timer myTimer;

		public int GenerationIntervalSec { get; set; }

		public string FilenameExtension { get; set; }

		public event EventHandler<FilenameGeneratedEventArgs> FilenameGenerated;

		public TimerFilenameGenerator()
		{
			myTimer = new Timer(OnIntervalElapsed, null, Timeout.Infinite, Timeout.Infinite);
		}

		public void Start()
		{
			myTimer.Change(0, this.GenerationIntervalSec * 1000);
		}

		public void Stop()
		{
			myTimer.Change(Timeout.Infinite, Timeout.Infinite);
		}

		private void OnIntervalElapsed(object theState)
		{
			FilenameGenerated?.Invoke(this, new FilenameGeneratedEventArgs(DateTime.Now, this.FilenameExtension));
		}
	}
}
