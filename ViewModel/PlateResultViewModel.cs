﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using openalprnet;

namespace OpenALPRDemo.ViewModel
{
	public class PlateResultViewModel : INotifyPropertyChanged
	{
		/// <summary>
		/// Result returned from OpenALPR.
		/// </summary>
		private AlprPlateResultNet myPlateResult;

		/// <summary>
		/// Recognition result with highest overall confidence.
		/// </summary>
		public AlprPlateNet BestPlate
		{
			get
			{
				return myPlateResult.BestPlate;
			}
		}

		/// <summary>
		/// List of other possible recognition results with lower overall confidences.
		/// </summary>
		public List<AlprPlateNet> TopNPlates
		{
			get
			{
				return myPlateResult.TopNPlates;
			}
		}

		/// <summary>
		/// Left coordinate of detected plate area.
		/// </summary>
		public int AreaLeft
		{
			get
			{
				int result = int.MaxValue;

				foreach (Point point in myPlateResult.PlatePoints)
				{
					result = Math.Min(result, point.X);
				}

				return result;
			}
		}

		/// <summary>
		/// Top coordinate of detected plate area.
		/// </summary>
		public int AreaTop
		{
			get
			{
				int result = int.MaxValue;

				foreach (Point point in myPlateResult.PlatePoints)
				{
					result = Math.Min(result, point.Y);
				}

				return result;
			}
		}

		/// <summary>
		/// Right coordinate of detected plate area.
		/// </summary>
		public int AreaRight
		{
			get
			{
				int result = 0;

				foreach (Point point in myPlateResult.PlatePoints)
				{
					result = Math.Max(result, point.X);
				}

				return result;
			}
		}

		/// <summary>
		/// Bottom coordinate of detected plate area.
		/// </summary>
		public int AreaBottom
		{
			get
			{
				int result = 0;

				foreach (Point point in myPlateResult.PlatePoints)
				{
					result = Math.Max(result, point.Y);
				}

				return result;
			}
		}

		/// <summary>
		/// Width of detected plate area.
		/// </summary>
		public int AreaWidth
		{
			get
			{
				return (this.AreaRight - this.AreaLeft);
			}
		}

		/// <summary>
		/// Left coordinate of plate area.
		/// </summary>
		public int AreaHeight
		{
			get
			{
				return (this.AreaBottom - this.AreaTop);
			}
		}

		/// <summary>
		/// Processing time in miliseconds.
		/// </summary>
		public float ProcessingTimeMs
		{
			get
			{
				return myPlateResult.ProcessingTimeMs;
			}
		}

		/// <summary>
		/// Property change notification event.
		/// </summary>
		/// <remarks>InotifyProperty implementation.</remarks>
		public event PropertyChangedEventHandler PropertyChanged;

		public PlateResultViewModel(AlprPlateResultNet theAlprPlateResult)
		{
			myPlateResult = theAlprPlateResult;
		}

		/// <summary>
		/// Invokes the property change event.
		/// </summary>
		/// <param name="propertyName">Name of the property value of which changed.</param>
		public void OnPropertyChanged(string propertyName)
		{
			// Raise the PropertyChanged event, passing the name of the property whose value has changed.
			this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
