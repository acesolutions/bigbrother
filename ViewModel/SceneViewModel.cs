﻿using openalprnet;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace OpenALPRDemo.ViewModel
{
	public class SceneViewModel : INotifyPropertyChanged
	{
		/// <summary>
		/// Full path to file containing the image.
		/// </summary>
		private string myFileName;

		/// <summary>
		/// List of license plates recognized in scene.
		/// </summary>
		private ObservableCollection<PlateResultViewModel> myLicensePlates = new ObservableCollection<PlateResultViewModel>();

		/// <summary>
		/// Currently selected license plate.
		/// </summary>
		private PlateResultViewModel mySelectedPlate;

		/// <summary>
		/// Path to scene image.
		/// </summary>
		public string FileName
		{
			get
			{
				return myFileName;
			}
			set
			{
				if (value != myFileName)
				{
					myFileName = value;
					DetectPlates();
					OnPropertyChanged("FileName");
				}
			}
		}

		/// <summary>
		/// License plates rocognized in scene.
		/// </summary>
		public ObservableCollection<PlateResultViewModel> LicensePlates
		{
			get
			{
				return myLicensePlates;
			}
		}

		/// <summary>
		/// Selected license plate.
		/// </summary>
		public PlateResultViewModel SelectedPlate
		{
			get
			{
				return mySelectedPlate;
			}
			set
			{
				if (value != mySelectedPlate)
				{
					mySelectedPlate = value;
					OnPropertyChanged("SelectedPlate");
				}
			}
		}

		/// <summary>
		/// Property change notification event.
		/// </summary>
		/// <remarks>InotifyProperty implementation.</remarks>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Creates an instane of scene view model.
		/// </summary>
		/// <param name="theSceneFileName">Path to the file containing the scene image.</param>
		public SceneViewModel(string theSceneFileName)
		{
			this.FileName = theSceneFileName;
		}

		/// <summary>
		/// Invokes the property change event.
		/// </summary>
		/// <param name="propertyName">Name of the property value of which changed.</param>
		public void OnPropertyChanged(string propertyName)
		{
			// Raise the PropertyChanged event, passing the name of the property whose value has changed.
			this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public void DetectPlates()
		{
			AlprNet alpr = new AlprNet("eu", @"openalpr.conf", @"runtime_data");
			if (!alpr.IsLoaded())
			{
				return;
			}

			// Optionally apply pattern matching for a particular region (US)
			alpr.DefaultRegion = "sk";

			AlprResultsNet results = alpr.Recognize(this.FileName);

			myLicensePlates.Clear();

			foreach (AlprPlateResultNet aPlate in results.Plates)
			{
				myLicensePlates.Add(new PlateResultViewModel(aPlate));
			}

			SavePlates();

			if (myLicensePlates.Count > 0)
			{
				SelectedPlate = myLicensePlates[0];
			}
		}

		public void SavePlates()
		{
			string filename = Path.ChangeExtension(myFileName, "txt");
			StreamWriter writer = new StreamWriter(filename);

			foreach(PlateResultViewModel aPlate in myLicensePlates)
			{
				writer.WriteLine(string.Format("{0} ({1}%)", aPlate.BestPlate.Characters, aPlate.BestPlate.OverallConfidence));
			}

			writer.Close();
		}
	}
}
