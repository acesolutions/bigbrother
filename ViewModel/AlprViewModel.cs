﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using openalprnet;

namespace OpenALPRDemo.ViewModel
{
	public class AlprViewModel : INotifyPropertyChanged
	{
		private string myVideoUrl;

		private ObservableCollection<SceneViewModel> myScenes = new ObservableCollection<SceneViewModel>();

		private SceneViewModel mySelectedScene;

		private object mySceneCollectionLock = new object();

		/// <summary>
		/// input video stream URL.
		/// </summary>
		public string VideoUrl
		{
			get
			{
				return myVideoUrl;
			}
			set
			{
				if (value != myVideoUrl)
				{
					myVideoUrl = value;
					OnPropertyChanged("VideoUrl");
				}
			}
		}

		/// <summary>
		/// Scenes captured from video stream..
		/// </summary>
		public ObservableCollection<SceneViewModel> Scenes
		{
			get
			{
				return myScenes;
			}
		}

		/// <summary>
		/// Selected scene.
		/// </summary>
		public SceneViewModel SelectedScene
		{
			get
			{
				return (mySelectedScene);
			}
			set
			{
				if (value != mySelectedScene)
				{
					mySelectedScene = value;
					OnPropertyChanged("SelectedScene");
				}
			}
		}

		/// <summary>
		/// Property change notification event.
		/// </summary>
		/// <remarks>InotifyProperty</remarks>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Creates an instance of ALPR view model.
		/// </summary>
		public AlprViewModel()
		{
			BindingOperations.EnableCollectionSynchronization(myScenes, mySceneCollectionLock);
		}

		/// <summary>
		/// Invokes the property change event.
		/// </summary>
		/// <param name="propertyName">Name of the property value of which changed.</param>
		public void OnPropertyChanged(string propertyName)
		{
			// Raise the PropertyChanged event, passing the name of the property whose value has changed.
			this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public void MakeScreenshot(string theFileName)
		{
			ProcessStartInfo psi = new ProcessStartInfo
			{
				FileName = Properties.Settings.Default.FFmpegPath,
				Arguments = string.Format("-i {0} -vframes 1 -q:v 1 {1}", Properties.Settings.Default.VideoStreamUrl, theFileName),
				CreateNoWindow = true,
				UseShellExecute = false,
			};

			Process process = Process.Start(psi);
			process.WaitForExit();
			SceneViewModel newScene = new SceneViewModel(theFileName);
			myScenes.Add(newScene);
			SelectedScene = newScene;
		}
	}
}
