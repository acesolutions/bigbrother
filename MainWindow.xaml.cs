﻿using Microsoft.Win32;
using System.Windows;
using OpenALPRDemo.ViewModel;
using OpenALPRDemo.View;
using System.IO;
using System;
using System.Windows.Controls;

namespace OpenALPRDemo
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private AlprViewModel myViewModel= new AlprViewModel();

		private TimerFilenameGenerator myTimerFilenameGenerator;

		public MainWindow()
		{
			InitializeComponent();
			myTimerFilenameGenerator = new TimerFilenameGenerator
			{
				FilenameExtension = ".jpg"
			};
			myTimerFilenameGenerator.FilenameGenerated += OnFilenameGenerated;
			this.DataContext = myViewModel;
		}

		private void SettingsButton_Click(object sender, RoutedEventArgs e)
		{
			SettingsView settingsWindow = new SettingsView();
			settingsWindow.ShowDialog();
		}

		private void StopButton_Click(object sender, RoutedEventArgs e)
		{
			myTimerFilenameGenerator.Stop();
		}

		private void StartButton_Click(object sender, RoutedEventArgs e)
		{
			myTimerFilenameGenerator.GenerationIntervalSec = Properties.Settings.Default.ScreenshotIntervalSec;
			myTimerFilenameGenerator.Start();
		}

		private void OnFilenameGenerated(object sender, FilenameGeneratedEventArgs e)
		{
			string filename = Path.Combine(Properties.Settings.Default.ScreenshotPath, e.Filename);
			myViewModel.MakeScreenshot(filename);
		}
	}
}
